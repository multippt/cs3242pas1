/**
 * Matrix operation functions
 * Based on: http://www.cg.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/teche23.html
 */
class MatrixFunction {
public:

	/**
	 * High-speed 4x4 determinant calculator
	 */
	double getDeterminant(double **mat) {
		double result = 0;
		result += getV(mat, 11, 22, 33, 44);
		result += getV(mat, 11, 23, 34, 42);
		result += getV(mat, 11, 24, 32, 43);

		result += getV(mat, 12, 21, 34, 43);
		result += getV(mat, 12, 23, 31, 44);
		result += getV(mat, 12, 24, 33, 41);

		result += getV(mat, 13, 21, 32, 44);
		result += getV(mat, 13, 22, 34, 41);
		result += getV(mat, 13, 24, 31, 42);

		result += getV(mat, 14, 21, 33, 42);
		result += getV(mat, 14, 22, 31, 43);
		result += getV(mat, 14, 23, 32, 41);

		result -= getV(mat, 11, 22, 34, 43);
		result -= getV(mat, 11, 23, 32, 44);
		result -= getV(mat, 11, 24, 33, 42);

		result -= getV(mat, 12, 21, 33, 44);
		result -= getV(mat, 12, 23, 34, 41);
		result -= getV(mat, 12, 24, 31, 43);

		result -= getV(mat, 13, 21, 34, 42);
		result -= getV(mat, 13, 22, 31, 44);
		result -= getV(mat, 13, 24, 32, 41);

		result -= getV(mat, 14, 21, 32, 43);
		result -= getV(mat, 14, 22, 33, 41);
		result -= getV(mat, 14, 23, 31, 42);

		return result;
	}

	// Get value with offset (to reduce odds of mistakes)
	double getV(double **mat, int xy) {
		int x = (xy / 10) - 1;
		int y = (xy % 10) - 1;
		return mat[x][y];
	}
	double getV(double **mat, int xy1, int xy2, int xy3, int xy4) {
		return getV(mat, xy1) * getV(mat, xy2) * getV(mat, xy3) * getV(mat, xy4);
	}

	double getV(double **mat, int xy1, int xy2, int xy3) {
		return getV(mat, xy1) * getV(mat, xy2) * getV(mat, xy3);
	}

	/**
	 * High-speed 4x4 inverse calculator
	 */
	bool getInverse(double **mat, double **output) {
		double det = getDeterminant(mat);
		if (det < 0.0000001 && det > -0.0000001) {
			return false;
		}

		output[0][0] = getV(mat, 22, 33, 44) + getV(mat, 23, 34, 42) + getV(mat, 24, 32, 43)
			- getV(mat, 22, 34, 43) - getV(mat, 23, 32, 44) - getV(mat, 24, 33, 42);

		output[0][1] = getV(mat, 12, 34, 43) + getV(mat, 13, 32, 44) + getV(mat, 14, 33, 42)
			- getV(mat, 12, 33, 44) - getV(mat, 13, 34, 42) - getV(mat, 14, 32, 43);

		output[0][2] = getV(mat, 12, 23, 44) + getV(mat, 13, 24, 42) + getV(mat, 14, 22, 43)
			- getV(mat, 12, 24, 43) - getV(mat, 13, 22, 44) - getV(mat, 14, 23, 42);

		output[0][3] = getV(mat, 12, 24, 33) + getV(mat, 13, 22, 34) + getV(mat, 14, 23, 32)
			- getV(mat, 12, 23, 34) - getV(mat, 13, 24, 32) - getV(mat, 14, 22, 33);

		output[1][0] = getV(mat, 21, 34, 43) + getV(mat, 23, 31, 44) + getV(mat, 24, 33, 41)
			- getV(mat, 21, 33, 44) - getV(mat, 23, 34, 41) - getV(mat, 24, 31, 43);

		output[1][1] = getV(mat, 11, 33, 44) + getV(mat, 13, 34, 41) + getV(mat, 14, 31, 43)
			- getV(mat, 11, 34, 43) - getV(mat, 13, 31, 44) - getV(mat, 14, 33, 41);

		output[1][2] = getV(mat, 11, 24, 43) + getV(mat, 13, 21, 44) + getV(mat, 14, 23, 41)
			- getV(mat, 11, 23, 44) - getV(mat, 13, 24, 41) - getV(mat, 14, 21, 43);

		output[1][3] = getV(mat, 11, 23, 34) + getV(mat, 13, 24, 31) + getV(mat, 14, 21, 33)
			- getV(mat, 11, 24, 33) - getV(mat, 13, 21, 34) - getV(mat, 14, 23, 31);

		output[2][0] = getV(mat, 21, 32, 44) + getV(mat, 22, 34, 41) + getV(mat, 24, 31, 42)
			- getV(mat, 21, 34, 42) - getV(mat, 22, 31, 44) - getV(mat, 24, 32, 41);

		output[2][1] = getV(mat, 11, 34, 42) + getV(mat, 12, 31, 44) + getV(mat, 14, 32, 41)
			- getV(mat, 11, 32, 44) - getV(mat, 12, 34, 41) - getV(mat, 14, 31, 42);

		output[2][2] = getV(mat, 11, 22, 44) + getV(mat, 12, 24, 41) + getV(mat, 14, 21, 42)
			- getV(mat, 11, 24, 42) - getV(mat, 12, 21, 44) - getV(mat, 14, 22, 41);

		output[2][3] = getV(mat, 11, 24, 32) + getV(mat, 12, 21, 34) + getV(mat, 14, 22, 31)
			- getV(mat, 11, 22, 34) - getV(mat, 12, 24, 31) - getV(mat, 14, 21, 32);

		output[3][0] = getV(mat, 21, 33, 42) + getV(mat, 22, 31, 43) + getV(mat, 23, 32, 41)
			- getV(mat, 21, 32, 43) - getV(mat, 22, 33, 41) - getV(mat, 23, 31, 42);

		output[3][1] = getV(mat, 11, 32, 43) + getV(mat, 12, 33, 41) + getV(mat, 13, 31, 42)
			- getV(mat, 11, 33, 42) - getV(mat, 12, 31, 43) - getV(mat, 13, 32, 41);

		output[3][2] = getV(mat, 11, 23, 42) + getV(mat, 12, 21, 43) + getV(mat, 13, 22, 41)
			- getV(mat, 11, 22, 43) - getV(mat, 12, 23, 41) - getV(mat, 13, 21, 42);

		output[3][3] = getV(mat, 11, 22, 33) + getV(mat, 12, 23, 31) + getV(mat, 13, 21, 32)
			- getV(mat, 11, 23, 32) - getV(mat, 12, 21, 33) - getV(mat, 13, 22, 31);

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				output[i][j] /= det;
			}
		}
		return true;
	}
};