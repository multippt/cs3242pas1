#ifndef _MESH_H_
#define _MESH_H_

#define _USE_MATH_DEFINES

using namespace std;

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <list>
#include <cmath>
#include <set>
#include <queue>
#include <unordered_map>
#include "Matrix.h"

typedef unsigned int uint;
typedef unsigned long long ulong;

typedef struct {
	//3d coordinates
	float x;
	float y;
	float z;
	uint id;
} Vertex;

typedef struct{
	uint a,b,c; //three vertex ids
	uint id;
	ulong edges[3];
	double plane[4]; // face plane values for error quadric
	double area;
	bool doDelete;
} Face;

typedef struct{
	uint v[2];
	ulong id;
	double heuristic;
	float preferredCoord[3];
	std::set<Face*> faces;
} Edge;

typedef struct {
	double m[4][4];
} Matrix;

class PairComparator {
public:
	bool operator() (const std::pair<double, Edge*>& p1, const std::pair<double, Edge*>& p2) {
		if (p1.first > p2.first) {
			return true;
		}
		else if (p1.first < p2.first) {
			return false;
		}
		else {
			return (p1.second->id > p2.second->id);
		}
	}
};

class Mesh{
public:
	Mesh() {};
	Mesh(const char*);
	//load a Mesh from .mesh file
	void loadMF(const char*);
	//write a Mesh to .mesh file (no header)
	void writeMF(const char*);
	//simplify a mesh
	void simplifyMesh(const char* input, const char* output, int faceCnt);
	//return vertex count
	int Vcnt();
	//return face count
	int Fcnt();
	
private:
	//Mesh operation
	void edgeCollapse(Edge* e);
	void edgeCollapseOld(Edge* e);
	void edgeCollapseOld2(Edge* e);

	ulong getEdgeID(uint v1, uint v2);

	//Mesh build
	void edgeAdd(ulong edgeID, Face* face);
	void edgeRemove(ulong edgeID, Face* face);
	void updateFace(Face* face);
	bool edgeExist(ulong edgeID);

	//Mesh query functions
	Edge* getEdge(ulong id);
	Edge* getBestEdge(bool& allowed);
	void computeHeuristic(Edge* e);
	Vertex* getVertex(uint id);
	void getVertices(Edge* e, std::vector<Vertex*> *vl);
	int getOneRingNeighbors(Vertex* v, std::vector<Face*> *fl);
	int getOneRingNeighbors(ulong edge, std::vector<Face*> *fl, bool computeShared = true);
	bool containsVertex(Face* f, Vertex* v);
	bool containsVertex(Face* f, uint v);
	bool containsVertex(Edge* e, uint v);

	// Error quadrics
	void updateQuadrics();
	void updateQuadric(Face* f);
	void getQuadric(Face* f, Matrix* m);
	void getQuadric(Edge* edge, double* result);
	double computeQuadric(Vertex* vertex, Matrix* Qmatrix);
	void getPlane(Face* f, Vertex* substituteVertex = NULL);
	bool getInverse(Matrix* source, Matrix* inverse);

	// Private variables
	MatrixFunction matrixFunction;
	std::vector<Vertex*> V;
	std::vector<Face*> F;
	std::vector<Edge*> E;
	std::unordered_map<ulong, Edge*> EMap;
	std::set<std::pair<double, Edge*>, PairComparator> pq; // heuristic queue
	int removedFaces;
	int resultCount;
	bool isLoading;
};
#endif