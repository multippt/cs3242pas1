#include "Mesh.h"

/**
 Kwan Yong Kang Nicholas (A0080933E)
 */

Mesh::Mesh(const char* filename) {
	loadMF(filename);
}

void Mesh::loadMF(const char* filename){
	// Rehash map for faster performance
	EMap.rehash(F.size() * 3);

	isLoading = true;
	if(V.size()>0) V.clear();
	if(F.size()>0) F.clear();
	std::ifstream infile;
	infile.open(filename, std::ios::in);
	std::string strbuff;
	while(std::getline(infile,strbuff)){
		std::stringstream ss;
		ss<<strbuff;
		char type;
		ss>>type;
		
		if(type=='v'){
			Vertex* v = new Vertex();
			ss>>v->x>>v->y>>v->z;
			v->id = V.size() + 1;
			V.push_back(v);
		}
		else if(type=='f'){
			Face* f = new Face();
			ss>>f->a>>f->b>>f->c;
			f->doDelete = false;
			f->id = F.size();

			// Generate mesh data
			updateFace(f);
			updateQuadric(f);

			F.push_back(f);
		}
	}

	isLoading = false;

	// Compute heuristic
	std::unordered_map<ulong, Edge*>::iterator ei;
	for (ei = EMap.begin(); ei != EMap.end(); ei++) {
		computeHeuristic(ei->second);
	}
	infile.close();
}

void Mesh::writeMF(const char* filename){
	std::ofstream outfile;
	outfile.open(filename, std::ios::out);
	//std::string strbuff;
	
	for(uint i=0;i<V.size();i++){
		outfile<<"v "<<V[i]->x<<" "<<V[i]->y<<" "<<V[i]->z<<std::endl;
	}
	for(uint i=0;i<F.size();i++){
		if (!F[i]->doDelete) {
			resultCount++;
			outfile << "f " << F[i]->a << " " << F[i]->b << " " << F[i]->c << std::endl;
		}
	}
	outfile.close();
}

void Mesh::simplifyMesh(const char* input, const char* output, int faceCnt){
	// Load mesh
	loadMF(input);
	std::cout << "Original face count: "<< F.size() << std::endl;

	// Perform simplification
	removedFaces = 0;
	int initial = F.size();
	bool allowed = false;
	while (initial - removedFaces > faceCnt) {
		Edge* e = getBestEdge(allowed); // Select desired edge based on heuristic
		if (e == NULL) { // In case no more edges to pick
			printf("No more edges to pick\n");
			break;
		}
		if (!allowed || e->v[0] == e->v[1] || e->faces.size() == 0) {
			continue; // Invalid edge, skip
		}
		edgeCollapse(e); // Perform edge collapse
	}

	// Save the mesh
	resultCount = 0;
	writeMF(output);
	std::cout<<"Result face count: "<<resultCount<<std::endl;
}

int Mesh::Vcnt(){
	return V.size();
}

int Mesh::Fcnt(){
	return F.size();
}

/**
 * Get the edge ID from two vertices
 */
ulong Mesh::getEdgeID(uint v1, uint v2) {
	ulong v1ul = v1;
	ulong v2ul = v2;
	if (v1ul > v2ul) {
		v1ul = v2;
		v2ul = v1;
	}
	ulong id = 0;
	id = v1ul << 32 | v2ul;
	return id;
}

/**
 * Perform edge collapse on the selected edge
 */
void Mesh::edgeCollapse(Edge* e) {
	std::vector<Face*> affectedFaces; // Pointers to actual faces
	int shared = getOneRingNeighbors(getEdgeID(e->v[0], e->v[1]), &affectedFaces);
	if (shared > 4 || e->faces.size() > 2) {
		return; // doing edge-collapse on this will cause non-manifold or will remove >2 faces
	}
	
	Vertex* mergedVertex = getVertex(e->v[0]);
	Vertex* deletedVertex = getVertex(e->v[1]);
	Vertex targetVertex;
	targetVertex.id = mergedVertex->id;

	// Get the final position of vertex
	targetVertex.x = e->preferredCoord[0];
	targetVertex.y = e->preferredCoord[1];
	targetVertex.z = e->preferredCoord[2];

	// Check for potential face flips (mesh inversion)
	for (uint i = 0; i < affectedFaces.size(); i++) {
		Face* f = affectedFaces.at(i);

		if (containsVertex(f, mergedVertex->id) && containsVertex(f, deletedVertex->id)) {
			continue; // skip faces that would be deleted.
		}

		// Simulate changing of vertices due to edge collapse
		Face copy = *f;
		if (copy.a == deletedVertex->id) {
			copy.a = mergedVertex->id;
		}
		if (copy.b == deletedVertex->id) {
			copy.b = mergedVertex->id;
		}
		if (copy.c == deletedVertex->id) {
			copy.c = mergedVertex->id;
		}
		getPlane(&copy, &targetVertex);

		// Find dot product
		double dotProduct = f->plane[0] * copy.plane[0] + f->plane[1] * copy.plane[1] + f->plane[2] * copy.plane[2];
		double angle = abs(acos(dotProduct)); // find angle
		if (angle >= M_PI/2) {
			return; // This causes a potential face flip, don't collapse the edge
		}
	}

	int deletedFaces = 0;

	// Assign final position
	mergedVertex->x = targetVertex.x;
	mergedVertex->y = targetVertex.y;
	mergedVertex->z = targetVertex.z;

	// Reassign faces using v2 to v1
	for (uint i = 0; i < affectedFaces.size(); i++) {
		Face* f = affectedFaces.at(i);
		if (f->a == deletedVertex->id) {
			f->a = mergedVertex->id;
		}
		if (f->b == deletedVertex->id) {
			f->b = mergedVertex->id;
		}
		if (f->c == deletedVertex->id) {
			f->c = mergedVertex->id;
		}

		if (f->a == f->b || f->b == f->c || f->c == f->a) {
			f->doDelete = true;
			removedFaces++;
			deletedFaces++;
		}

		// Fix up edge connections on faces
		updateFace(f);
	}

	if (deletedFaces != 2) {
		printf("Deleted %d faces\n", deletedFaces);
	}

	// Find all edges that need to be updated
	std::set<ulong> updateEdges;
	for (uint i = 0; i < affectedFaces.size(); i++) {
		Face* f = affectedFaces.at(i);

		// Update face quadric
		updateQuadric(f);

		if (!f->doDelete) {
			updateEdges.insert(f->edges[0]);
			updateEdges.insert(f->edges[1]);
			updateEdges.insert(f->edges[2]);
		}
	}

	// Update heuristic
	std::set<ulong>::iterator seti;
	for (seti = updateEdges.begin(); seti != updateEdges.end(); seti++) {
		computeHeuristic(getEdge(*seti));
	}
}

/**
 * Checks if an edge contains the vertex
 */
bool Mesh::containsVertex(Edge* e, uint v) {
	if (e == NULL) {
		return false;
	}
	return (e->v[0] == v) || (e->v[1] == v);
}

/**
 * Checks if a face contains the vertex
 */
bool Mesh::containsVertex(Face* f, uint v) {
	return (f->a == v) || (f->b == v) || (f->c == v);
}

/**
 * Get a vertex by id
 */
Vertex* Mesh::getVertex(uint id) {
	return V.at(id - 1); // Assuming no vertices deleted
}

/**
 * Get vertices attached to an edge
 */
void Mesh::getVertices(Edge* e, std::vector<Vertex*> *vl) {
	vl->push_back(getVertex(e->v[0]));
	vl->push_back(getVertex(e->v[1]));
}

/**
 * Get one-ring neighbors around edge
 */
int Mesh::getOneRingNeighbors(ulong edge, std::vector<Face*> *fl, bool computeShared) {
	Edge* e;
	Edge* base = getEdge(edge);
	std::list<Edge*> queue;
	std::set<uint> visitedFaces;
	std::set<ulong> visitedEdges;
	visitedEdges.insert(base->id);
	queue.push_back(base);

	std::set<uint> verticesWithV1; // vertices associated with faces using v1
	std::set<uint> verticesWithV2; // vertices associated with faces using v1

	while (!queue.empty()) {
		e = queue.front();
		queue.pop_front();
		std::set<Face*>::iterator i = e->faces.begin();
		while (i != e->faces.end()) {
			Face* f = *i;
			if (f->doDelete) {
				i++;
				continue;
			}
			if (containsVertex(f, base->v[0]) || containsVertex(f, base->v[1])) {
				if (visitedFaces.find(f->id) == visitedFaces.end()) {
					visitedFaces.insert(f->id);
					fl->push_back(f);

					// Keep track of vertices
					if (containsVertex(f, base->v[0])) {
						verticesWithV1.insert(f->a);
						verticesWithV1.insert(f->b);
						verticesWithV1.insert(f->c);
					}
					if (containsVertex(f, base->v[1])) {
						verticesWithV2.insert(f->a);
						verticesWithV2.insert(f->b);
						verticesWithV2.insert(f->c);
					}

					// Add edges to queue
					for (int i = 0; i < 3; i++) {
						Edge* ei = getEdge(f->edges[i]);
						if (containsVertex(ei, base->v[0]) || containsVertex(ei, base->v[1])) {
							if (visitedEdges.find(ei->id) == visitedEdges.end()) {
								visitedEdges.insert(ei->id);
								queue.push_back(ei);
							}
						}
					}
				}
			}
			i++;
		}
	}

	if (!computeShared) {
		return 0;
	}

	// Find number of vertices shared between the two one-ring neighbors
	int sharedCount = 0;
	std::set<uint>::iterator vi = verticesWithV1.begin();
	std::set<uint>::iterator vj = verticesWithV2.begin();
	std::set<uint> sharedVertices;
	
	while (vi != verticesWithV1.end() && vj != verticesWithV2.end()) {
		if (*vi == *vj) {
			sharedVertices.insert(*vi);
			sharedCount++;
			vi++;
			vj++;
			continue;
		}

		if (*vi < *vj && vi != verticesWithV1.end()) {
			vi++;
			continue;
		}
		if (*vj < *vi && vj != verticesWithV2.end()) {
			vj++;
			continue;
		}
	}

	return sharedCount;
}

/**
 * Get the one-ring neighbor around vertex (deprecated)
 */
int Mesh::getOneRingNeighbors(Vertex* v, std::vector<Face*> *fl) {
	// Prevent adding of duplicates
	std::set<uint> unique;
	uint i;
	for (i = 0; i < fl->size(); i++) {
		Face* f = fl->at(i);
		unique.insert(f->id);
	}

	for (i = 0; i < F.size(); i++) {
		Face* f = F.at(i);
		if (f->doDelete) {
			continue;
		}
		if (unique.find(f->id) == unique.end()) {
			if (containsVertex(f, v)) {
				fl->push_back(f);
			}
		}
	}
	return 0;
}

/**
 * Check if a face contains the vertex
 */
bool Mesh::containsVertex(Face* f, Vertex* v) {
	// Get vertex from ID
	int i = v->id;
	return (f->a == i || f->b == i || f->c == i);
}

/**
 * Get the edge by its id
 */
Edge* Mesh::getEdge(ulong id) {
	if (EMap.find(id) == EMap.end()) {
		return NULL;
	}
	return EMap[id];
}

/**
 * Add the face to the edge
 */
void Mesh::edgeAdd(ulong edgeID, Face* face) {
	Edge* e;
	std::unordered_map<ulong, Edge*>::iterator i = EMap.find(edgeID);
	if (i == EMap.end()) {
		e = new Edge();
		ulong v1 = edgeID & 0xFFFFFFFF;
		ulong v2 = edgeID >> 32;
		e->v[0] = (uint)v1;
		e->v[1] = (uint)v2;
		e->id = edgeID;
		EMap[edgeID] = e;
	} else {
		e = i->second;
	}
	e->faces.insert(face);
}

/**
 * Remove the face from the edge
 */
void Mesh::edgeRemove(ulong edgeID, Face* face) {
	if (edgeID == 0) {
		return;
	}
	Edge* e;
	std::unordered_map<ulong, Edge*>::iterator i = EMap.find(edgeID);
	if (i == EMap.end()) {
		return; // error
	} else {
		e = i->second;
	}
	e->faces.erase(face);
}

/**
 * Checks if the edge exist
 */
bool Mesh::edgeExist(ulong edgeID) {
	if (EMap.find(edgeID) == EMap.end()) {
		return false;
	} else {
		return true;
	}
}

/**
 * Update face edges
 */
void Mesh::updateFace(Face* f) {
	if (f->doDelete) {
		edgeRemove(f->edges[0], f);
		edgeRemove(f->edges[1], f);
		edgeRemove(f->edges[2], f);
		return;
	}

	ulong oldEdges[3];
	oldEdges[0] = f->edges[0];
	oldEdges[1] = f->edges[1];
	oldEdges[2] = f->edges[2];

	// Recompute edges
	f->edges[0] = getEdgeID(f->a, f->b);
	f->edges[1] = getEdgeID(f->b, f->c);
	f->edges[2] = getEdgeID(f->c, f->a);

	if (oldEdges[0] != f->edges[0]) {
		edgeRemove(oldEdges[0], f);
		edgeAdd(f->edges[0], f);
	}
	if (oldEdges[1] != f->edges[1]) {
		edgeRemove(oldEdges[1], f);
		edgeAdd(f->edges[1], f);
	}
	if (oldEdges[2] != f->edges[2]) {
		edgeRemove(oldEdges[2], f);
		edgeAdd(f->edges[2], f);
	}
}

/**
 * Update heuristic for the edge
 */
void Mesh::computeHeuristic(Edge* e) {
	double heuristic = 0;
	double lastHeuristic = e->heuristic;
	std::pair<double, Edge*> oldCandidate(lastHeuristic, e);

	getQuadric(e, &heuristic);
	e->heuristic = heuristic;
	
	if (pq.find(oldCandidate) == pq.end()) {
		pq.insert(std::pair<double, Edge*>(heuristic, e));
	} else {
		// Replace
		pq.erase(oldCandidate);
		pq.insert(std::pair<double, Edge*>(heuristic, e));
	}
}

/**
 * Rebuild entire priority queue (test function)
 */
void Mesh::updateQuadrics() {
	pq.clear();
	//std::map<ulong, Edge*>::iterator i;
	std::unordered_map<ulong, Edge*>::iterator i;
	for (i = EMap.begin(); i != EMap.end(); i++) {
		if (i->second->faces.size() > 0) {
			computeHeuristic(i->second);
		}
	}
}

/**
 * Get the best edge according to heuristic
 */
Edge* Mesh::getBestEdge(bool& allowed) {
	// The smaller values are picked first
	Edge* e = pq.rbegin()->second;
	double heuristic = pq.rbegin()->first;
	allowed = !(e == NULL || abs(e->heuristic - heuristic) > 0.00000001);
	if (!allowed) {
		int z = 0;
	}
	std::set<std::pair<double, Edge*>, PairComparator>::iterator i = pq.end();
	i--;
	pq.erase(i);
	return e;
}

/**
 * Get plane of a face
 */
void Mesh::getPlane(Face* f, Vertex* substituteVertex) {
	Vertex* a = getVertex(f->a);
	Vertex* b = getVertex(f->b);
	Vertex* c = getVertex(f->c);

	// Substitute relevant vertex
	if (substituteVertex != NULL) {
		if (a->id == substituteVertex->id) {
			a = substituteVertex;
		}
		if (b->id == substituteVertex->id) {
			b = substituteVertex;
		}
		if (c->id == substituteVertex->id) {
			c = substituteVertex;
		}
	}

	double AB[3] = { (b->x - a->x), (b->y - a->y), (b->z - a->z) };
	double AC[3] = { (c->x - a->x), (c->y - a->y), (c->z - a->z) };

	// Find normal
	double normal[3];
	normal[0] = AB[1] * AC[2] - AB[2] * AC[1];
	normal[1] = -(AB[0] * AC[2] - AB[2] * AC[0]);
	normal[2] = AB[0] * AC[1] - AB[1] * AC[0];

	// Normalize normal vector
	double normalDist = sqrt(normal[0] * normal[0] + normal[1] * normal[1] + normal[2] * normal[2]);
	f->area = normalDist / 2;
	normal[0] /= normalDist;
	normal[1] /= normalDist;
	normal[2] /= normalDist;

	// Assign plane information
	f->plane[0] = normal[0];
	f->plane[1] = normal[1];
	f->plane[2] = normal[2];
	f->plane[3] = -(a->x * normal[0] + a->y * normal[1] + a->z * normal[2]);
}

void Mesh::updateQuadric(Face* f) {
	getPlane(f);
}

/**
 * Get the Kp matrix associated with a face
 */
void Mesh::getQuadric(Face* f, Matrix* m) {
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			m->m[i][j] = f->plane[i] * f->plane[j]; // * f->area;
		}
	}
}

/**
 * Calculate the quadric heuristic for this vertex
 */
double Mesh::computeQuadric(Vertex* vertex, Matrix* Qmatrix) {
	double quadric = 0;
	double vert[4] = { vertex->x, vertex->y, vertex->z, 1.0 };
	double intermediate[4] = { 0, 0, 0, 0 };

	// Matrix multiplication v' * Kp
	for (int mi = 0; mi < 4; mi++) {
		for (int mj = 0; mj < 4; mj++) {
			intermediate[mi] += vert[mj] * Qmatrix->m[mi][mj];
		}
	}

	// Matrix multiplication (v' * Kp) * v
	for (int mi = 0; mi < 4; mi++) {
		quadric += intermediate[mi] * vert[mi];
	}

	if (quadric < 0) {
		quadric = 0;
	}

	return quadric;
}

/**
 * Get the error quadric heuristic and find the final vertex position
 */
void Mesh::getQuadric(Edge* edge, double* result) {
	std::vector<Face*> ringNeighbors;
	Matrix KpMatrix, QMatrix;

	getOneRingNeighbors(edge->id, &ringNeighbors, false);

	// Set quadric matrix to null matrix
	for (int mi = 0; mi < 4; mi++) {
		for (int mj = 0; mj < 4; mj++) {
			QMatrix.m[mi][mj] = 0;
		}
	}

	/*
	// This one simulates the error quadrics in the paper (it has overlapping faces)
	for (int vi = 0; vi < 2; vi++) {
		for (uint i = 0; i < ringNeighbors.size(); i++) {
			Face* f = ringNeighbors.at(i);
			if (f->doDelete) { // Exclude deleted faces
				continue;
			}

			if (!containsVertex(f, edge->v[vi])) {
				continue;
			}

			getQuadric(f, &KpMatrix); // Get Kp of each plane

			// Sum of all the Kp is Q
			for (int mi = 0; mi < 4; mi++) {
				for (int mj = 0; mj < 4; mj++) {
					QMatrix.m[mi][mj] += KpMatrix.m[mi][mj];
				}
			}
		}
	}*/

	
	// Compute quadric matrix Q for the edge using faces affected by the edge collapse
	for (uint i = 0; i < ringNeighbors.size(); i++) {
		Face* f = ringNeighbors.at(i);
		if (f->doDelete) { // Exclude deleted faces
			continue;
		}

		getQuadric(f, &KpMatrix); // Get Kp of each plane

		// Sum of all the Kp is Q
		for (int mi = 0; mi < 4; mi++) {
			for (int mj = 0; mj < 4; mj++) {
				QMatrix.m[mi][mj] += KpMatrix.m[mi][mj];
			}
		}
	}

	double lowestQuadric;
	Vertex target;
	Vertex* vertices[2] = { getVertex(edge->v[0]), getVertex(edge->v[1]) };

	// Find optimal final vertex position
	Matrix partialQuadric; // copy first 3 rows of quadric matrix
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 3; j++) {
			partialQuadric.m[i][j] = QMatrix.m[i][j];
		}
	}
	// Set last row as [0 0 0 1]
	partialQuadric.m[0][3] = 0;
	partialQuadric.m[1][3] = 0;
	partialQuadric.m[2][3] = 0;
	partialQuadric.m[3][3] = 1;

	// Find inverse and solve for final vertex position
	Matrix quadricInverse;
	float preferredCoord[3];
	if (getInverse(&partialQuadric, &quadricInverse)) {
		double base[4] = { 0, 0, 0, 1 };
		double result[4];
		for (int i = 0; i < 4; i++) {
			result[i] = 0;
			for (int j = 0; j < 4; j++) {
				result[i] += quadricInverse.m[j][i] * base[j];
			}
		}
		target.x = preferredCoord[0] = (float)result[0];
		target.y = preferredCoord[1] = (float)result[1];
		target.z = preferredCoord[2] = (float)result[2];
		// result[3] should equal to 1

		lowestQuadric = computeQuadric(&target, &QMatrix); // Compute quadric value
	} else { // Can't solve for best final vertex, so guess final vertex position
		int cases = 3; // number of points to consider along edge

		// Find lowest quadric amongst these cases
		for (int i = 0; i < cases; i++) {
			float alpha = (float)i / (cases - 1);
			target.x = vertices[0]->x * alpha + vertices[1]->x * (1 - alpha);
			target.y = vertices[0]->y * alpha + vertices[1]->y * (1 - alpha);
			target.z = vertices[0]->z * alpha + vertices[1]->z * (1 - alpha);

			double quadric = computeQuadric(&target, &QMatrix);
			if (i == 0 || quadric < lowestQuadric) {
				lowestQuadric = quadric;
				preferredCoord[0] = target.x;
				preferredCoord[1] = target.y;
				preferredCoord[2] = target.z;
			}
		}
	}
	
	// Assign coordinates of final vertex
	edge->preferredCoord[0] = preferredCoord[0];
	edge->preferredCoord[1] = preferredCoord[1];
	edge->preferredCoord[2] = preferredCoord[2];

	// Divide by amount of faces affected by the edge (from experience it gives improved results)
	lowestQuadric /= ringNeighbors.size();

	*result = lowestQuadric;
}

/**
 * Gets the matrix inverse, if any
 */
bool Mesh::getInverse(Matrix* source, Matrix* inverse) {
	double* mat[4];
	mat[0] = source->m[0];
	mat[1] = source->m[1];
	mat[2] = source->m[2];
	mat[3] = source->m[3];
	
	double* invmat[4];
	invmat[0] = inverse->m[0];
	invmat[1] = inverse->m[1];
	invmat[2] = inverse->m[2];
	invmat[3] = inverse->m[3];
	return matrixFunction.getInverse(mat, invmat);
}